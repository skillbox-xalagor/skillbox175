#include <iostream>

class Opinion
{
public:
	Opinion() : OpinionText("Opinion")
	{}
	std::string GetOpinion() {
		return OpinionText;
	}
	int SetOpinion() {
		//�� ������ ������
		return 0; //���������� ��� ��������� ����������
	}
private:
	std::string OpinionText;
};

class Vector
{
public:
	Vector() : x(5), y(5), z(5)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z;
	}
	double Length()
	{
		return sqrt(x * x + y * y + z * z);
	}
private:
	double x = 0;
	double y = 0;
	double z = 0;
};

int main()
{
	Vector v;

	//���������� ������ � ��� �����
	std::cout << "Vector:";
	v.Show();
	std::cout << "\nVector length: " << v.Length() << '\n';

	Opinion op;

	//���������� ������ ������ ������
	std::cout << op.GetOpinion();

	return 0;
}